## promise的介绍和基本使用

Promise是异步编程的一种解决方案

回调地狱

```js
// 1 .使用setTimeOUT
      // setTimeout(() => {
      //   console.log("Hello world!");
      // }, 1000);

      // 参数 -》 函数
      // resolve ,reject本身它们本身又是函数
      // 链式编程
      new Promise((resolve, reject) => {
        // 1. 第一次网络请求的代码
        setTimeout(() => {
          resolve();
        }, 1000);
      }).then(() => {
        // 第一次拿到结果的代码
        console.log("Hello World");

        // 2. 第二次网络请求的代码
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve();
          }, 1000);
        }).then(() => {
          // 第二次处理的代码
          console.log("Hello Vuejs!");

          //3. 第三次请求的代码
          return new Promise((resolve, reject) => {
            setTimeout(() => {
              resolve();
            }, 1000);
          }).then(() => {
            // 第三次处理的代码
            console.log("Hello Python!");
          });
        });
      });
      // 什么情况下会用到Promise?
      // 一般情况下是有异步操作时，使用Promise对这个异步操作进行封装
      // new -> 构造函数(1.保存一些状态信息， 2. 执行传入的函数)
      // 在执行传入的回调函数时，会传入两个参数，resolve, reject，本身又是函数
      new Promise((resolve, reject) => {
        setTimeout(() => {
          // 将网络的请求和处理进行分离
          // 成功的时候
          resolve("Hello World");

          //失败的时候调用rejcet
          reject("error message");
        }, 1000);
      })
        .then((data) => {
          // 1. 100行的处理代码 优雅啊
          console.log(data);
          console.log(data);
          console.log(data);
          console.log(data);
          console.log(data);
        })
        .catch((err) => {
          // 处理错误情况
          console.log(err);
        });
```

## Promise三种状态和另外一种写法

```js
new Promise((resolve, reject) => {
        setTimeout(() => {
          // resolve("Hello Vuejs");
          reject("error message");
        }, 1000);
      }).then(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
```

## Promise链式调用

```js
// 网络请求： aaa -> 自己处理(10行)
      // 处理： aaa111 -> 自己处理(10行)
      // 处理： aaa1112222 -> 自己处理
      // new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve("aaa");
      //   }, 1000);
      // })
      //   .then(() => {
      //     // 1. 自己处理10行代码
      //     console.log(res, "第一层的10行处理代码");

      //     // 2. 对结果进行第一次的处理
      //     return new Promise((resove) => {
      //       resolve(res + "111");
      //     });
      //   })
      //   .then((res) => {
      //     console.log(res, "第二层的10行处理代码");

      //     return new Promise((resolve) => {
      //       resolve(res + "222");
      //     }).then((res) => {
      //       console.log(res, "第三层的10行处理代码");
      //     });
      //   });

      // new Promise(resolve => resolve(结果))简写
      // new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve("aaa");
      //   }, 1000);
      // })
      //   .then((res) => {
      //     // 1. 自己处理10行代码
      //     console.log(res, "第一层的10行处理代码");

      //     // 2. 对结果进行第一次的处理
      //     return Promise.resolve(res + "111");
      //   })
      //   .then((res) => {
      //     console.log(res, "第二层的10行处理代码");

      //     return Promise.resolve(res + "222");
      //   })
      //   .then((res) => {
      //     console.log(res, "第三层的10行处理代码");
      //   });

      // 省略掉promise,resolve
      // new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve("aaa");
      //   }, 1000);
      // })
      //   .then((res) => {
      //     // 1. 自己处理10行代码
      //     console.log(res, "第一层的10行处理代码");

      //     // 2. 对结果进行第一次的处理
      //     return res + "111";
      //   })
      //   .then((res) => {
      //     console.log(res, "第二层的10行处理代码");

      //     return res + "222";
      //   })
      //   .then((res) => {
      //     console.log(res, "第三层的10行处理代码");
      //   });

      // reject
      new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve("aaa");
        }, 1000);
      })
        .then((res) => {
          // 1. 自己处理10行代码
          console.log(res, "第一层的10行处理代码");

          // 2. 对结果进行第一次的处理
          // return Promise.reject("error message");
          throw "error message"; // 手动抛出
        })
        .then((res) => {
          console.log(res, "第二层的10行处理代码");

          return Promise.resolve(res + "222");
        })
        .then((res) => {
          console.log(res, "第三层的10行处理代码");
        })
        .catch((err) => {
          console.log(err);
        });
```

手写Promises 可以来一手

## Promise的all方法的调用

```js
// 请求1:
      // let isResult1 = false;
      // let isResult2 = false;
      // $ajax({
      //   url: "",
      //   success: function () {
      //     console.log("结果1");
      //     isResult1 = true;
      //     handleResult();
      //   },
      // });

      // 结果2:
      // $ajax({
      //   url: "",
      //   success: function () {
      //     console.log("结果2");
      //     isResult2 = true;
      //     handleResult();
      //   },
      // });

      // function handleResult() {
      //   if (isResult1 && isResult2) {
      //     //
      //   }
      // }
// 异步请求同步调用
Promise.all([
        // new Promise((resolve, reject) => {
        //   $ajax({
        //     url: "url1",
        //     success: function (data) {
        //       resolve(data);
        //     },
        //   });
        // }),
        // new Promise((resolve, reject) => {
        //   $ajax({
        //     url: "url2",
        //     success: function (data) {
        //       resolve(data);
        //     },
        //   });
        // }),

        new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve("result1");
          }, 2000);
        }),

        new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve("result2");
          }, 1000);
        }),
      ]).then((results) => {
        console.log(results[0]);
        console.log(results[1]);
      });

```

