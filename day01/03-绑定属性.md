# Vue.js coderwhy笔记之绑定属性

## v-bind的基本使用

前面我们学的指令主要是将值插入到我们模板的内容当中

但是，除了内容需要动态来决定外，某些属性我们也希望动态来帮顶

	+ 比如动态绑定a元素的href属性
	+ 比如动态绑定img元素的src属性
这个时候我们就可以使用v-bind指令
	1. 作用：动态绑定属性
	2. 缩写: `:`x
	3. 预期：any(with argument)|Object (without argument)
	4. 参数：attrOrProp(option)

案例：分别给img和a，动态绑定src和href
```html
<div id="app">
      <!-- 错误的做法：这里不可以使用mustache语法 -->
      <!-- <img src=" {{imgURL}} " alt="" /> -->
      <!-- 正确的做法：使用v-bind指令 -->
      <img v-bind:src="imgURL" alt="" />
      <!-- 语法糖写法 : -->
      <a :href="aHref">百度一下</a>
    </div>

    <script src="../js/vue.js"></script>

    <script>
      const app = new Vue({
        el: "#app",
        data: {
          message: "你好",
          imgURL: "https://pics.images.ac.cn/image/5f20ff3cf0363.html",
          aHref: "http://www.baidu.com",
        },
      });
    </script>
```

## v-bind动态绑定class(对象语法)

案例：使用一个按钮来动态切换文字的颜色

```html
<style>
      .active {
        color: pink;
      }
    </style>
  </head>
  <body>
    <div id="app">
      <!-- <h2 class="active">{{message}}</h2> -->
      <!-- <h2 :class="active">{{message}}</h2> -->

      <!-- <h2 :class="{类名: boolean值}"> {{message}} </h2> -->
      <!-- 通过布尔值来控制，在console里面控制app.isActive来控制 -->
      <!-- 也可以自己添加静态class,两个class会合并 -->
      <h2 class="title" :class="{active: isActive,line: false}">{{message}}</h2>
      <!-- 在按钮里绑定事件动态改变 -->
      <button @click="changeColor">按钮</button>
    </div>

    <script src="../js/vue.js"></script>

    <script>
      const app = new Vue({
        el: "#app",
        data: {
          message: "你好",
          active: "active",
          isActive: false
        },
        methods: {
          changeColor: function() {
            this.isActive = !this.isActive
          }
        }
      });
    </script>
  </body>
```

.class后面跟的是一个对象

1. 直接{{}}绑定一个类
2. 通过判断传入多个值
3. 和普通的类同时存在不冲突
4. 如果过于复杂，可以放在一个methods或者computed中

## v-bind动态绑定class(数组语法) 用的少

.class后面跟的是一个数组

```html
<div id="app">
      <h2 class="title" :class=" [active, 'line'] ">{{message}}]</h2>
    </div>

    <script src="../js/vue.js"></script>

    <script>
      const app = new Vue({
        el: "#app",
        data: {
          message: "你好",
          active: "aaaa",
        },
      });
    </script>
```

## 作业：结合v-for和v-bind来完成动态切换颜色

<https://cn.vuejs.org/v2/guide/events.html#%E5%86%85%E8%81%94%E5%A4%84%E7%90%86%E5%99%A8%E4%B8%AD%E7%9A%84%E6%96%B9%E6%B3%95>

参考这个来实现，可以通过事件来获取当前项的index

答案有，但是还是建议独立完成

## v-bind动态绑定style（对象语法）

案例，通过动态绑定style来实现样式

```html
<div id="app">
      <!-- <h2 :style="{key(属性名): value(属性值)}">{{message}}</h2> -->
      <!-- 要加上单引号 value当作对象来解析-->
      <!-- <h2 :style="{fontSize: '50px'}">{{message}}</h2> -->
      <!-- finalSize当成变量来使用 -->
      <h2 :style="{fontSize: finalSize + 'px',color: finalColor}">
        {{message}}
      </h2>
      <!-- 做一个抽取 -->
      <h2 :style="getStyle">
        {{message}}
      </h2>
    </div>

    <script src="../js/vue.js"></script>

    <script>
      const app = new Vue({
        el: "#app",
        data: {
          message: "你好",
          finalSize: 100,
          finalColor: "red",
        },
        methods: {
          getStyle: function () {
            return { fontSize: this.finalSize + "px", color: this.finalColor };
          },
        },
      });
    </script>
```



## v-bind动态绑定style（对象语法）

```html
<div id="app">
      <h2 :style=" [baseStyle1,baseStyle2] ">{{message}}</h2>
    </div>

    <script src="../js/vue.js"></script>

    <script>
      const app = new Vue({
        el: "#app",
        data: {
          message: "你好",
          baseStyle1: { backgroundColor: "red" },
          baseStyle2: { fontSize: "100px" },
        },
      });
    </script>
```
