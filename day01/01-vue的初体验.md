# Vue.js coderwhy笔记之vue初体验

## 简单认识vue.js

1. Vue的读音  (读音 /vjuː/，类似于 **view**)

2. Vue是一个渐进式的框架

   - 可以把Vue作为应用的一部分嵌入其中带来更丰富的交互体验

   - 想要使用更多的业务逻辑采用Vue实现，可以使用Vue的核心库及其生态系统

   - 比如Core+Vue-router+Vuex，也可以满足你的各种各样的需求

3. Vue有很多特点和Web开发中常见的高级功能

   + 解耦视图和数据
   + 可复用的组件
   + 前端路由技术
   + 状态管理
   + 虚拟DOM
4. 这些特点，后面随着学习的深入，会慢慢体会到的
5. 学习Vue.js的前提？
	+ 不需要Angular,React和jQuery的基础
	+ 需要HTML，CSS，JavaScript基础

官网地址：<https://cn.vuejs.org/v2/guide/index.html>

## Vue的安装方式
安装的方式：
1. 直接CDN引入
开发环境：
```html
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```
​				生产环境
```html
<!-- 生产环境版本，优化了尺寸和速度 -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```

2. 下载和引入
    <https://cn.vuejs.org/v2/guide/installation.html>
    
      开发版本 vue.js
    
      生产版本 vue.js.min
    
3. NPM安装

  + 后续通过webpack和CLI的使用

实操：在项目中引入vue.js

![image-20200728180623496](https://pics.images.ac.cn/image/5f2148ed27b1d.html)

## Hello Vuejs

需求：用vue在页面输出一个‘你好哇，李银河’

```html
<div id="app">{{message}}</div>
    <!-- 没有经过Vue管理 -->
    <div>{{message}}</div>

    <script src="../js/vue.js"></script>

    <script>
        // vue.js里面创建了Vue这个function，所以能够new
        // let(变量)/const(常量)

        //编程范式：声明式编程==》数据与界面分离
        const app = new Vue({
            el: "#app", // 用于挂载要管理的元素
            data: {
                //定义数据
                message: "你好哇，李银河",
            },
        });

        // 元素js的做法（编程范式：命令式编程）
        // 1.创建div元素，设置id属性
        // 2. 定义一个变量叫做message
        // 3.将message变量放在前面的div元素中显示

        // 4.修改mesasge数据
        // 5.将修改后的数据放入div元素

        // 在f12进行调试。console更改message的值
    </script>
```

![image-20200728182754832](https://pics.images.ac.cn/image/5f21490c94350.html)



##### 创建Vue对象的时候，传入了一些options:{}

 + {}中包含了el属性：该属性决定了这个Vue对象挂载到哪一个元素上

 + 包含了data属性：该属性通常会存储一些数据

   	+ 这些数据可以是我们直接定义出来的
      	+ 也可以是请求服务器得到的

## Vue列表展示

```html
    <div id="app">
        <ul>
            <li v-for="item in movies">{{ item }}</li>
        </ul>
    </div>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: "#app",
            data: {
                movies: ["星际穿越", "大话西游", "少年派", "盗梦空间"],
            },
        });
    </script>
```

![image-20200728183954078](http://graph.baidu.com/resource/122c60091797f44847e8001596016934.jpg)



1. 在HTML中，我们采用v-for指令
2. 响应式操作

## 案例：计数器

```html
 <div id="app">
        <h2>当前计数： {{counter}}</h2>
        <!-- <button v-on:click="counter++">+</button> -->
        <!-- <button v-on:click="counter--">-</button> -->
        <button v-on:click="add">+</button>
        <button v-on:click="sub">-</button>
    </div>

    <script src="../js/vue.js"></script>
    <script>
        const app = new Vue({
            el: "#app",
            data: {
                counter: 0,
            },
            methods: {
                add: function() {
                    console.log("add会执行");
                    // 不能直接引用counter，才能引用app当中的counter  代理
                    this.counter++;
                },
                sub: function() {
                    console.log("sub会执行");
                    this.counter--;
                },
            },
        });

        // 1 拿button元素

        // 2 添加监听事件
    </script>
```

1. 我们实现了一个小的计数器
	+ 点击+ 计数器+1
	+ 点击-  计数器-1

	![image-20200729031857023](http://graph.baidu.com/resource/1228aca5998107c122bf001596016961.jpg)
	
	
	
2. 我们使用了新的指令和属性

   1. 新的属性：methods，该属性用于在VUe对象中定义方法
   2. 新的指令：@click(v-on:click的语法糖)，该指令用于监听某个元素的点击事件，当需要制定当发生点击时，执行方法
## Vue中的MVVM
1. 维基百科，放在根目录下了
2. Vue中的MVVM

![image-111](https://user-gold-cdn.xitu.io/2018/6/24/1643060c121f7361?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)
3. 计数器来具体说明



![image-112](http://graph.baidu.com/resource/122ca2d11dbaa9f596b1f01596016990.jpg)

## Vue的options选项

官方api文档 <https://cn.vuejs.org/v2/api/>

1. el:
   1. 类型：string|HTMLElement （document.querySelector() 跟jquery还有响应式源码有关）
   2. 作用：决定Vue实例会管理哪一个DOM
2. data:
   1. 类型：Object|Function（组件当中data是函数）
   2. 作用：Vue实例对应的数据类型
3. methods:
   1. 类型：{[key:string]:Function}）（后面可用ES6优化）
   2. 作用：定义属于Vue的一些方法，可以在其他地方调用，也可以在指令中调用
关于方法与函数的区别：
	+ 类里面的叫做方法
	+ 放在类外面的叫做函数
## 什么是Vue的生命周期
生命周期：事物从诞生到消亡的整个过程
github上tags当中才是稳定的版本

![image-20200729035722738](http://graph.baidu.com/resource/122a2ece451a3b374d18b01596017017.jpg)

![image-20200729102445309](http://graph.baidu.com/resource/1221647e84d9598d90e5201596017049.jpg)

vue.js源码

![image-20200729102600864](http://graph.baidu.com/resource/1225bff586a6969a27b0001596017073.jpg)

## Vue的生命周期函数有哪些

created中主要完成网页请求，初始化页面

![Vue 实例生命周期](https://cn.vuejs.org/images/lifecycle.png)

## 定义vue的template

规范：缩进两个空格更规范

在prettier的json中配置

在设置-代码片段-全局代码片段里面添加

注意`""`需要用\转义

```json
{ 
  	"print to vue-template": {
		"prefix": "myvue",
		"body": [
			"<div id=\"app\">{{message}}</div>",
      "",
      "<script src=\"../js/vue.js\"></script>",
      "",
      "<script>",
      "",
      "  const app = new Vue({",
      "    el: \"#app\",",
      "    data: {",
      "      message: \"你好\",",
      "    },",
      "  });",
      "</script>",
		],
		"description": "vue"
	}
}
```

`prefix`中的内容就是以后触发代码块的内容了

