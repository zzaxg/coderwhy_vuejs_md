## 动态路由的使用

```js
{
    path: "/user/:userId",
    component: User
  }
```



```vue
<router-link :to="'/user/' + userId">用户</router-link>

 data() {
    return {
      userId: "lisi",
    };
  },
```

```vue
<template>
  <div>
    <h2>我是用户界面</h2>
    <p>我是用户相关信息</p>
    <h2>{{userId}}</h2>
  </div>
</template>

<script>
export default {
  name: "User",
  computed: {
    userId() {
       // 获取当前路径的参数
      return this.$route.params.userId;
    },
  },
};
</script>
```

## 打包文件的解析

路由的懒加载

dist中的js，app页面逻辑，manifest底层支撑，vendor第三方

```js
// 路由懒加载
const Home = () => import("../components/Home.vue");
const About = () => import("../components/About.vue");
const User = () => import("../components/User.vue");
```

打包之后，js多了三个组件

## 嵌套路由

```js
{
    path: "/",
    // redirect重定向
    redirect: "/home"
  },
  {
    path: "/home",
    component: Home,
    children: [
      {
        path: "",
        redirect: "news"
      },
      {
        path: "news", // 省略/
        component: HomeNews
      },
      {
        path: "message", // 省略/
        component: HomeMessage
      }
    ]
  },
      
   const HomeNews = () => import("../components/HomeNews.vue");
const HomeMessage = () => import("../components/HomeMessage.vue");
```

```vue
<template>
  <div>
    <h2>我是首页</h2>
    <p>我是首页内容</p>
    <router-link to="/home/news">新闻</router-link>
    <router-link to="/home/message">消息</router-link>
    <router-view></router-view>
  </div>
</template>
```

## router参数传递

params的方式

```js
  {
    path: "/user/:userId",
    component: User
  },
```

```vue
<router-link :to="'/user/' + userId">用户</router-link>
```



通过query传递参数

方式一：(<router-link>)

```vue
<template>
  <div>
    <h2>我是profile</h2>
    <h2>{{$route.query.name}}</h2>
    <h2>{{$route.query.age}}</h2>
    <h2>{{$route.query.height}}</h2>
  </div>
</template>

<router-link :to="{path: '/profile', query: {name: 'why', age:18, height: 1.80}}">档案</router-link>
```

方式二：

自定义的按钮(js代码)

```vue
<button @click="userClick">用户</button>
<button @click="profileClick">档案</button>

userClick() {
      this.$router.push("/user/" + this.userId);
    },
    profileClick() {
      this.$router.push({
        path: "/profile",
        query: {
          anem: "zhouzhou",
          age: 19,
          height: 1.88,
        },
      });
    },
```

## router和route的由来

```vue
methods: {
    btnClick() {
      // 所有组件都继承自Vue类的原型
      console.log(this.$router);
      console.log(this.$route);
      this.test();
      console.log(this.$name);
    },
  },
```

```js
Vue.prototype.test = function() {
  console.log("test");
};

Vue.prototype.$name = "zhouzhou";

```

## 全局导航守卫

```js
router.beforeEach((to, from, next) => {
  // 从from跳转到to
  document.title = to.matched[0].meta.title;
  next();
});

const routes = [
  // 比较特殊，放在最前面
  {
    path: "/",
    // redirect重定向
    redirect: "/home"
  },
  {
    path: "/home",
    component: Home,
    children: [
      {
        path: "",
        redirect: "news"
      },
      {
        path: "news", // 省略/
        component: HomeNews
      },
      {
        path: "message", // 省略/
        component: HomeMessage
      }
    ],
    meta: {
      title: "首页"
    }
  },
  {
    path: "/about",
    component: About,
    meta: {
      title: "关于"
    }
  },
  {
    path: "/user/:userId",
    component: User,
    meta: {
      title: "用户"
    }
  },
  {
    path: "/profile",
    component: Profile,
    meta: {
      title: "档案"
    }
  }
];
```

定义一个meta 使用match解决嵌套的问题

<https://router.vuejs.org/zh/guide/advanced/navigation-guards.html#%E5%85%A8%E5%B1%80%E5%89%8D%E7%BD%AE%E5%AE%88%E5%8D%AB>

## keep-alive

```vue
<keep-alive>
  <router-view />
</keep-alive>
```

```js
data() {
    return {
      message: "", 
      path: "/home/news", // 记录当前路径
    };
  },


// 这个函数在keep-alive存在时才是有效的
  activated() {
    // console.log("activated");
    this.$router.push(this.path);
  },

  beforeRouteLeave(to, from, next) {
    // ...
    console.log(this.$route.path);
    this.path = this.$route.path;
    next();
  },
```

属性：

+ include:
+ exclude: 排除在外 字符串或者正则

```vue
    <!-- 这里引用的是组件的name 不能加空格 正则也不能加空格 \d{2,9}-->
    <keep-alive exclude="Profile, User">
      <router-view />
    </keep-alive>
```

