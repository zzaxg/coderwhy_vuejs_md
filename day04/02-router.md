## 前后端渲染和路由

1. 后端路由：后端处理URL和页面之间的映射关系 SEO优化

2. 前后端分离：后端之负责提供数据，不负责任何阶段的内容

   前端渲染：浏览器中显示的网页的大部分内容，都是由前端写的js代码在浏览器中执行，最终渲染出来的代码
   
3. SPA页面: 整个网站只有一个html页面  把全部资源全部加载进来 然后根据url来选择性的渲染

## Url的hash和HTML history

location.hash = 'bbb'

history.pushState({}, '', 'home')

history.go(-1) = history.back()

history.forward) = history.go(-1)

## vue-router的安装和配置

#### vue-router是基于路由和组件的

+ 路由用于设定访问路径，将路径和组件映射起来
+ 在vue-router的单页面应用中，页面的路径的改变就是组件的切换

#### 安装路由

1.安装

npm install vue-router --save

2.在模块化工程中使用它（因为它是一个插件，所以可以通过Vue.use()来安装路由功能）

```js
// 配置路由相关的信息
import VueRouter from "vue-router";
import Vue from "vue";

// 1.通过Vue.use(插件)，安装插件
Vue.use(VueRouter);

const routes = [];

// 2.创建路由对象
const router = new VueRouter({
  // 配置路由和组建的应用关系
  routes
});

// 3. 将router对象传入与Vue实例中
export default router;
```

```js
// 目录下会自动寻找index
import router from "./router";

/* eslint-disable no-new */
new Vue({
  el: "#app",
  // 挂载到router
  router,
  render: h => h(App)
});
```

#### 使用路由

1. 创建路由组件
2. 配置路由映射，组件和路径的映射关系
3. 使用路由：通过<router-link>和<router-view>

```js
import Home from "../components/Home.vue";
import About from "../components/About.vue";

const routes = [
  {
    path: "/home",
    component: Home
  },
  {
    path: "/about",
    component: About
  }
];
```

```vue
<template>
  <div id="app">
    <router-link to="/home">首页</router-link>
    <router-link to="/about">关于</router-link>
    <router-view></router-view>
  </div>
</template>
```

```vue
<template>
  <div>
    <h2>我是首页</h2>
    <p>我是首页内容</p>
  </div>
</template>

<script>
export default {
  name: "Home",
};
</script>

<style>
</style>
```

```vue
<template>
  <div>
    <h2>我是关于</h2>
    <p>我是关于内容</p>
  </div>
</template>

<script>
export default {
  name: "About",
};
</script>

<style>
</style>
```

#### 路由的默认值和修改为history模式

+ <router-link>该标签是一个vue-router中已经内置的组件，它会被渲染成一个<a>标签
+ <rouer-view>该标签会根据当前的路径，动态渲染出不同的组件
+ 网页的其他内容，比如顶部的标题/导航，或者底部的一些版权信息等回合<router-view>处于同一个等级
+ 在路由切换时，切换的是<router-view>挂载的组件，其他内容不会发生改变

##### 路由的默认路径:

```js
{
    path: "/",
    // redirect重定向
    redirect: "/home"
  },
```

+ 我们在routes中又配置了一个映射
+ path配置的是根路径: /
+ redirect是重定向，也就是我们将根路径重定向到/home的路径下

```js
const router = new VueRouter({
  // 配置路由和组建的应用关系
  routes,
  mode: "history" // html5 history模式 消除井号
});
```

#### <router-link>一些补充

+ tag:可以指定<router-link>渲染成什么组件
+ repalce:让页面不能返回 不会留下history记录
+ active-class: 当<router-link>对应的路由匹配成功的时候，会自动给当前元素设置一个router-link-active的class，这是active-class可以修改默认的名称

#### 通过代码跳转路由

```vue
 <button @click="homeClick">首页</button>
  <button @click="aboutClick">关于</button>

methods: {
    homeClick() {
      // 通过代码的方式修改路由 vue-router
      console.log("home");
      // push => pushState
      // this.$router.push("/home");
      this.$router.replace("/home");
    },
    aboutClick() {
      console.log("about");
      // this.$router.push("/about");
      this.$router.replace("/about");
    },
  },
```

