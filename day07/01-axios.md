## axios框架的基本使用

vue init webpack testaxios

npm install axios --save

```js
import axios from "axios";

axios({
  url: "http://152.136.185.210:8000/api/z8/home/multidata",
  method: "get"
}).then(res => {
  console.log(res);
});

axios({
  // url: "http://152.136.185.210:8000/api/z8/home/data?type=sell&page=3",
  url: "http://152.136.185.210:8000/api/z8/home/data",
  // 专门针对get请求的参数拼接
  params: {
    type: "pop",
    page: 1
  },
  method: "get"
}).then(res => {
  console.log(res);
});
```

## axios发送并发请求

```js
// 2. axios发送并发请求
axios
  .all([
    axios({
      url: "http://152.136.185.210:8000/api/z8/home/multidata"
    }),
    axios({
      url: "http://152.136.185.210:8000/api/z8/home/data",
      params: {
        type: "sell",
        page: 3
      }
    })
  ])
  .then(results => {
    console.log(results);
    console.log(results[0]);
    console.log(results[1]);
  });

axios
  .all([
    axios({
      url: "http://152.136.185.210:8000/api/z8/home/multidata"
    }),
    axios({
      url: "http://152.136.185.210:8000/api/z8/home/data",
      params: {
        type: "sell",
        page: 3
      }
    })
  ])
  .then(
    // 使用axios.spread将我们的数组展开
    axios.spread((res1, res2) => {
      console.log(res1);
      console.log(res2);
    })
  );
```

## axios的配置信息

```js
// 全局配置
axios.defaults.baseURL = "http://152.136.185.210:8000/api/z8";
axios.defaults.timeout = 5000;

axios
  .all([
    axios({
      url: "/home/multidata"
    }),
    axios({
      url: "/home/data",
      params: {
        type: "sell",
        page: 3
      }
    })
  ])
  .then(
    // 使用axios.spread将我们的数组展开
    axios.spread((res1, res2) => {
      console.log(res1);
      console.log(res2);
    })
  );
```

## axios实例

```js
//4.创建对应的aixos实例
const instance1 = axios.create({
  baseURL: "http://152.136.185.210:8000/api/z8",
  timeout: 5000
});

instance1({
  url: "/home/multidata"
}).then(res => {
  console.log(res);
});

instance1({
  url: "/home/data",
  params: {
    type: "sell",
    page: 3
  }
}).then(res => {
  console.log(res);
});

const instance2 = axios.get({
  baseURL: "http://152.136.185.210:8000/api/z8",
  timeout: 8000
  // headers: {}
});

```

## 模块的封装

```js
export function request(config) {
  const instacne = axios.create({
    baseURL: "http://152.136.185.210:8000/api/z8",
    timeout: 5000
  });

  // 发送真正的网络请求
  return instacne(config); // 返回值本来就是Promise
}



import { request } from "./network/request";

request({
  url: "/home/multidata"
})
  .then(res => {
    console.log(res);
  })
  .catch(err => {
    console.log(err);
  });
```

